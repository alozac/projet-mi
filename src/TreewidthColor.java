import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;

public class TreewidthColor extends ColorGraph {

	boolean[] alreadySeen;
	int   nbOrigV;
	
	public TreewidthColor(TreeDecomposition treeDec, int nbCol) {
		super(treeDec, nbCol);
		alreadySeen = new boolean[nbV];
		Arrays.fill(alreadySeen, false);
		nbOrigV = treeDec.getNbVertOrigGraph();
	}
	
	public TreewidthColor(TreeDecomposition treeDec) {
		this(treeDec, 3);
	}
						
	
	public boolean colorGraph(boolean showSol) {
		BagVertex bag = (BagVertex) graph.getVertices().get(0);
		bag.setSOL(SOL(bag));
		ArrayList<int[]>sol = bag.getSOL();
		if(sol.size() == 0){
			return false;
		} else {
			System.out.println(sol.size() + " colorations");
			if (showSol){
				int[]solu = bag.getSOL().get(0);
				fillColorations(bag, solu);
				for(int i = 0; i< solu.length;i++){
					((TreeDecomposition)graph).getGraph().getById(i).setColor(solu[i]);
				}
				for(int i = 0; i < solu.length;i++){
					System.out.println(i+" : "+colors[solu[i]]);
				}
			}
			return true;
		}
	}
	
	private boolean isFilled(int[] colo){
		for (int i = 0;i< colo.length;i++){
			if (colo[i]==-1){
				return false;
			}
		}
		return true;
	}
	
	private void fillColorations(BagVertex bag, int[]colo) {
		boolean [] alreadySeen = new boolean[nbV];
		Arrays.fill(alreadySeen, false);
		ArrayDeque<BagVertex> fifo = new ArrayDeque<BagVertex>();
		fifo.push(bag);
		alreadySeen[0]=true;
		while(isFilled(colo) == false && !fifo.isEmpty()){

			BagVertex curr = fifo.pop();
		
			ArrayList<AbstractVertex> nb = curr.getNgd();

			for(int j = 0 ; j < nb.size() ; j++){
				if(!alreadySeen[nb.get(j).getId()]){
					alreadySeen[nb.get(j).getId()] = true;
					fifo.push((BagVertex) nb.get(j));
				}
			}
		
			int[] colo2 = getCompatibleColo(colo, curr.getSOL());
			
			for (int i = 0; i<colo.length;i++){
				if(colo[i] == -1 && colo2[i] != -1){
					colo[i]=colo2[i];
				}
			}
		}
		for (int i = 0; i < colo.length;i++){
			if(colo[i] == -1){
				colo[i] = 0;
			}
		}
		
		
}
	
	private int[] getCompatibleColo(int[] colo, ArrayList<int[]> sol) {
		boolean compat;
		for (int[] colSol : sol){
			compat = true;
			for (int i = 0 ; i < colSol.length ; i++){
				if (colo[i] != -1 && colSol[i] != -1 && colSol[i] != colo[i]){
					compat = false;
					break;
				}
			}
			if (compat) return colSol;
		}
		return null;
	}


	
	
	public ArrayList<int[]> SOL(BagVertex b){
		ArrayList<int[]> sol = COL(b);
		ArrayList<int[]> copy = new ArrayList<int[]>();
		copy.addAll(sol);
		alreadySeen[b.getId()] = true;
		for(AbstractVertex a : b.getNgd()){
			if(alreadySeen[a.getId()] == false){
				ArrayList<Vertex> inter = inter(b,(BagVertex) a);
				ArrayList<int[]> solSon = SOL((BagVertex)a);
				((BagVertex)a).setSOL(solSon);
				for(int[] x : copy){
					if (!colorationCompatible(x, solSon, inter)){
						sol.remove(x);
					}
				}
			}
			
		}
		return sol;
	}
	
	public ArrayList<int[]> COL(BagVertex X){
		ArrayList<int[]> col = new ArrayList<int[]>();
		
		ArrayList<Vertex> elem = X.getElements();
		int sizeX = X.sizeBag();
		
		for (int i = 0 ; i < pow(sizeX, 3) ; i++){
			
			int [] colo = buildColo(i, elem, sizeX);
			
			boolean colPos = true;
			for (Edge e : X.getInclEdges()){
				if (colo[e.u.getId()] == colo[e.v.getId()]){
					colPos = false;
					break;
				}
			}
			if (colPos)
				col.add(colo);
		}
		return col;
	}
	
	public boolean colorationCompatible(int[] col, ArrayList<int[]> solSon, ArrayList<Vertex> inter){
		for(int[] col2 : solSon){
			boolean colPos = true;
			for(Vertex v : inter){
				if(col[v.id] != col2[v.id]){
					colPos = false;
					break;
				}
			}
			if (colPos) 
				return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Vertex> inter (BagVertex b, BagVertex a){
		ArrayList<Vertex> inters = (ArrayList<Vertex>) b.getElements().clone();
		inters.retainAll(a.getElements());
		return inters;
	}
	
	public int[] buildColo(int nthCol, ArrayList<Vertex> elemBag, int sizeBag){
		StringBuilder s = new StringBuilder(Integer.toString(nthCol, nbCol));
		int lenS = s.length();
		for (int j = 0; j < sizeBag - lenS;j++){
			s.insert(0, '0');  
		}
		
		int[] colo = new int[nbOrigV];
		Arrays.fill(colo, -1);
		for (int j = 0 ; j < sizeBag ; j++){
			int ident = elemBag.get(j).getId();
			colo[ident] = s.charAt(j) - '0';
		}
		return colo;
	}
	

	private int pow(int exp, int sizeBag) {
		int x = sizeBag;
		for (int i = 1 ; i < exp ; i++){
			sizeBag *= x;
		}
		return sizeBag;
	}
	
	public void affichTab(int[]tab){
		for(int i = 0; i < tab.length;i++){
			System.out.print((tab[i] == -1?"#":tab[i])+" ");
		}
		System.out.println();
	}

}
