import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class Main {
	
    protected static String styleSheet;

	public static void main(String[] args) {
		long start, end;
		boolean isCol;
		ColorGraph col;

		
		int nbCol = 3;
		boolean showSol = true;
		
		String fileName = "medium1";
		
		Graph graph = new Graph(fileName);
		
//		System.out.println("Naif : ");
//		start = System.currentTimeMillis();
//		col   = new NaifColor(graph, nbCol);
//		isCol = col.colorGraph(showSol);
//		end   = System.currentTimeMillis();
//		System.out.println(  (!isCol ? "false": "") + 
//				 " ( " + (end - start) + "ms )\n");
		
	
		
		System.out.println("Sat : ");
		col   = new SatColor(graph, nbCol);
		start = System.currentTimeMillis();
		isCol = col.colorGraph(showSol);
		end   = System.currentTimeMillis();
		System.out.println(  (!isCol ? "false": "") + 
							 " ( " + (end - start) + "ms )\n");


//		System.out.println("Treewidth : ");
//		TreeDecomposition treeDec = new TreeDecomposition(graph, fileName);
//		col   = new TreewidthColor(treeDec, nbCol);
//		start = System.currentTimeMillis();
//		isCol = col.colorGraph(showSol);
//		end   = System.currentTimeMillis();
//		System.out.println(  (!isCol ? "false": "") + 
//				 " ( " + (end - start) + "ms )\n");
		

		
		buildStyleSheet();
		display(graph, true);
	//	display(treeDec, true);
	}

	private static void display(AbstractGraph graph, boolean autoPos) {
		SingleGraph g = new SingleGraph("graphe");
		g.addAttribute("ui.stylesheet", styleSheet);
		g.addAttribute("ui.quality");
		g.addAttribute("ui.antialias");
		
		for (AbstractVertex v : graph.getVertices()){
			Node n = g.addNode(v.toString());
			if (v instanceof BagVertex)
				n.addAttribute("ui.label", ((BagVertex) v).getElements());
			else n.addAttribute("ui.label", v.toString());
			
			if (!autoPos){
				int x = 100;
				n.setAttribute("xyz",Math.random()*x, Math.random()*x, Math.random()*x);
			}
			
			n.setAttribute("ui.class", v.getColor() == -1 ? "node"
														  : ColorGraph.colors[v.getColor()]);
		}
		for (Edge e : graph.getEdges()){
			g.addEdge(e.toString(), e.u.getId(), e.v.getId());
		}
		
		g.display(autoPos);
	}
	
	
	private static void buildStyleSheet() {
		styleSheet = "node {" +
	            "	fill-color: black;" +
	            "}";
		String color;
		for (int i = 0 ;  i < ColorGraph.colors.length ; i++){
			color = ColorGraph.colors[i];
			styleSheet += "node." + color + "{" +
						  "	fill-color: " + color + ";" +
						  "}";
		}
	}
	

}
