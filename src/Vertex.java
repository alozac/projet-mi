import java.util.ArrayList;
import java.util.List;

public class Vertex extends AbstractVertex{
	
	private List<Vertex> succ = new ArrayList<Vertex>();
	
	public Vertex(int id) {
		super(id);
		color=-1;
	}
	

	
	public String toString(){
		return id+"";
	}

	public List<Vertex> getSucc() {
		return succ;
	}

	public void addSucc(Vertex succ) {
		this.succ.add(succ);
	}
	
	public void clearSucc(){
		color = -1;
		for (Vertex v : succ){
			v.clearSucc();
		}
		succ.clear();
	}
}
