import java.util.ArrayList;
import java.util.List;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

public class SatColor extends ColorGraph {
	
	public SatColor(Graph graph) {
		super(graph, 3);
	}
	
	public SatColor(Graph graph, int nbCol) {
		super(graph, nbCol);
	}
	
	public boolean colorGraph(boolean showSol) {
		List<int[]> cnf = buildCnf();
		int[] solution = solve(cnf);
		if (solution != null){
			if(showSol){
				for (int i = 0 ; i < solution.length ; i++){
					if (solution[i] < 0) continue;
					int[] var = decodeFromIdVar(solution[i]);
					graph.getById(var[0] - 1).setColor(var[1] - 1);
					System.out.println((var[0] - 1) + " : " + colors[var[1] - 1]);
				}
			}
			return true;
		}
		return false;
	}
	
	private int[] solve(List<int[]> cnf){
		int maxVar = (nbV + 1) * 10;
		int nbClauses = cnf.size();

		ISolver solver = SolverFactory.newDefault();

		solver.newVar(maxVar);
		solver.setExpectedNumberOfClauses(nbClauses);
		
		try{
			for (int i = 0 ; i < nbClauses ; i++) {
			  int[] clause = cnf.get(i);
			  solver.addClause(new VecInt(clause));
			}

			IProblem problem = solver;
			
			if (problem.isSatisfiable()) {
				return problem.model();
			} else {
				return null;
			}
		} catch (ContradictionException e) {
	        System.out.println("Unsatisfiable (trivial)!");
	    } catch (TimeoutException e) {
	        System.out.println("Timeout, sorry!");
	    }
		return null;
	}
	
	private int codeToIdVar(int vertex, int color){
		return vertex * 10 + color;
	}
	
	private int[] decodeFromIdVar(int idVar){
		int[] dec = new int[2];
		dec[0] = idVar / 10;
		dec[1] = idVar % 10;
		return dec;
	}
	
	private void existenceConstraint(List<int[]> cnf){
		int[] clause;
		for (int i = 1 ; i <= nbV ; i++){
			clause = new int[nbCol];
			for (int j = 1 ; j <= nbCol ; j++){
				clause[j-1] = codeToIdVar(i, j);
			}
			cnf.add(clause);
		}
	}
	
	private void unicityConstraint(List<int[]> cnf){
		int[] clause;
		for (int i = 1 ; i <= nbV ; i++){
			for (int j = 1 ; j <= nbCol ; j++){
				for (int k = j+1 ; k <= nbCol ; k++){
					clause = new int[2];
					clause[0] = -codeToIdVar(i, j);
					clause[1] = -codeToIdVar(i, k);
					cnf.add(clause);
				}
			}
		}
	}
	
	private void adjacenceConstraint(List<int[]> cnf){
		int[] clause;
		for (int i = 1 ; i <= nbV ; i++){
			for (int j = i+1 ; j <= nbV ; j++){
				if (isEdge(i - 1, j - 1)){
					for (int k = 1 ; k <= nbCol ; k++){
						clause = new int[2];
						clause[0] = -codeToIdVar(i, k);
						clause[1] = -codeToIdVar(j, k);
						cnf.add(clause);
					}
				}
			}
		}
	}
	
	
	private boolean isEdge(int u, int v){
		for (Edge e : graph.getEdges())
			if ( e.u.getId() == u && e.v.getId() == v
			  || e.v.getId() == u && e.u.getId() == v) return true;
		return false;
	}

	private List<int[]> buildCnf(){
		List<int[]> cnf= new ArrayList<int[]>();
		unicityConstraint(cnf);
		existenceConstraint(cnf);
		adjacenceConstraint(cnf);
		return cnf;
	}
}