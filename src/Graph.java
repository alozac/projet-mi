import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Graph extends AbstractGraph{
	
	public Graph (String s){
		super();
		File f=new File(s + ".txt");
		try {
			BufferedReader br = new BufferedReader(
									new FileReader(f));

			String str="";
			str=readNextLine(br);
			String [] st=str.split(" ");
			nbVer=Integer.parseInt(st[0]);
			nbEdge=Integer.parseInt(st[1]);
			for (int j = 0; j<nbVer; j++){
				vertices.add(new Vertex(j));
			}
			for(int i = 0; i < nbEdge; i++){
				str=readNextLine(br);
				st=str.split(" ");
				Vertex u = (Vertex) vertices.get(Integer.parseInt(st[0]));
				Vertex v = (Vertex) vertices.get(Integer.parseInt(st[1]));
				edges.add(new Edge(u, v));		
				u.addNgh(v);
				v.addNgh(u);
			}
			br.close();
			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} 
	}
	
	public String toString(){
		String s = "Je suis un graphe normal. ";
		s += super.toString();
		s += "Aretes : \n";
		for (Edge e : edges)
			s += e + "\n";
		return s;
	}

	
	
	
}
