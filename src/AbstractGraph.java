import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGraph {
	protected int nbVer;
	protected int nbEdge;
	
	protected ArrayList<AbstractVertex> vertices;
	protected ArrayList<Edge> edges;

	public AbstractGraph() {
		vertices = new ArrayList<AbstractVertex>();
		 edges   = new ArrayList<Edge>();
	}
	
	protected String readNextLine(BufferedReader br) {
		try {
			String line;
			while ((line = br.readLine()).contains("#"));
			return line;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<AbstractVertex> getVertices(){
		return vertices;
	}
	public AbstractVertex getById(int id){
		return vertices.get(id);
	}
	
	public List<Edge> getEdges(){
		return edges;
	}
	
	public String toString(){
		return("J'ai "+nbVer+" sommets et "+nbEdge+" aretes.\n");
	}
	


}
