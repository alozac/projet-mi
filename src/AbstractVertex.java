import java.util.ArrayList;

public class AbstractVertex {

	protected int id;
	protected ArrayList<AbstractVertex> ngd;
	protected int color;
	
	public AbstractVertex(int id) {
		this.id = id;
		ngd = new ArrayList<AbstractVertex>();
	}
	
	public int getId(){
		return id;
	}
	
	public void addNgh(AbstractVertex v){
		ngd.add(v);
	}
	
	public ArrayList<AbstractVertex> getNgd(){
		return ngd;
	}
	
	public int getColor(){
		return color;
	}
	
	public String toString(){
		return ""+id;
	}
	public void setColor(int colid){
		color = colid;
	}

}
