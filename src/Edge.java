
public class Edge {
	public AbstractVertex u;
	public AbstractVertex v;

	public Edge(AbstractVertex u, AbstractVertex v) {
		this.u=u;
		this.v=v;
	}
	
	public String toString(){
		return "( " + u.getId() + " , " + v.getId() + " )";
	}

}
