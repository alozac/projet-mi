import java.util.ArrayList;

public class BagVertex extends AbstractVertex {
	
	private ArrayList<Vertex> elements;
	private ArrayList<Edge> inclEdges;
	
	private ArrayList<int[]> SOL;

	public BagVertex(int id) {
		super(id);
		elements  = new ArrayList<Vertex>();
		inclEdges = new ArrayList<Edge>();
	}
	
	public ArrayList<Edge> getInclEdges() {
		return inclEdges;
	}
	
	public void addEdge(Edge newEdge) {
		inclEdges.add(newEdge);
	}

	public void addElement(Vertex newVert) {
		elements.add(newVert);
	}
	
	public boolean containsVertex(Vertex v){
		return elements.contains(v);
	}
	
	public int sizeBag(){
		return elements.size();
	}
	
	public ArrayList<Vertex> getElements() {
		return elements;
	}
	
	public ArrayList<int[]> getSOL() {
		return SOL;
	}

	public void setSOL(ArrayList<int[]> sOL) {
		SOL = sOL;
	}
	
	public String toString(){
		String s = "[ " + elements.get(0);
		for (int i = 1 ; i < elements.size() ; i++)
			s += ", " + elements.get(i);
		s += " ]  ,  { " + (inclEdges.size() > 0 ? inclEdges.get(0) : "");
		for (int i = 1 ; i < inclEdges.size() ; i++)
			s += ", " + inclEdges.get(i);
		return s + " }";
	}

}
