
public abstract class ColorGraph {
	
	protected AbstractGraph graph;
	protected Integer nbV;
	
	protected static String[] colors = {"Pink", "Purple", "Cyan"};
	protected int nbCol;
	
	public ColorGraph(AbstractGraph graph, int nbCol){
		this.graph = graph;
		this.nbV   = graph.getVertices().size();
		this.nbCol = nbCol;
	}
	
	abstract boolean colorGraph(boolean showSol);
}