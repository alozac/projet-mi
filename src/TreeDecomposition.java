import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class TreeDecomposition extends AbstractGraph {
	private Graph graph;
	
	public TreeDecomposition(String s) {
		this(new Graph(s), s);
	}
	
	public TreeDecomposition(Graph graph, String s) {
		super();
		this.setGraph(graph);
		File f = new File(s + "Dec.txt");
		try {
			BufferedReader br = new BufferedReader(
									new FileReader(f));

			String [] st = readNextLine(br).split(" ");
			nbVer  = Integer.parseInt(st[0]);
			nbEdge = Integer.parseInt(st[1]);
			
			BagVertex tmp;
			Vertex tmpV;
			for (int j = 0 ; j < nbVer ; j++){
				tmp = new BagVertex(j);
				for (String elem : readNextLine(br).split(" ")){
					tmpV = (Vertex) graph.getById(Integer.parseInt(elem));
					tmp.addElement(tmpV);
				}
				vertices.add(tmp);
			}
			
			for(int i = 0; i < nbEdge ; i++){
				st = readNextLine(br).split(" ");
				BagVertex u = (BagVertex) vertices.get(Integer.parseInt(st[0]));
				BagVertex v = (BagVertex) vertices.get(Integer.parseInt(st[1]));
				edges.add(new Edge(u, v));		
				u.addNgh(v);
				v.addNgh(u);
			}
			
			BagVertex bag;
			for (Edge e : graph.getEdges()){
				for (AbstractVertex ab : vertices){
					bag = (BagVertex) ab;
					if (bag.containsVertex((Vertex) e.u) &&
						(bag.containsVertex((Vertex) e.v)))
						bag.addEdge(e);
				}
			}
			br.close();
			
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		} 
	}
	
	public Graph getOrigGraph(){
		return getGraph();
	}
	
	public int getNbVertOrigGraph(){
		return getGraph().getVertices().size();
	}
	
	public String toString(){
		String s = "Je suis une arborescence. ";
		s += super.toString();
		s += "Sommets : \n";
		for (int i = 0 ; i < vertices.size() ; i++)
			s += i +" : " + vertices.get(i) + "\n";
		s += "Aretes : \n";
		for (Edge e : edges)
			s += e + "\n";
		s += "Je suis l'arborescence de : \n\t" + getGraph().toString();
		return s;
	}

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
	

}
