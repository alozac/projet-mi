import java.util.Arrays;
import java.util.List;

public class NaifColor extends ColorGraph {
	
	
	public NaifColor(Graph graph) {
		super(graph, 3);
	}
	
	public NaifColor(Graph graph, int nbCol) {
		super(graph, nbCol);
	}

	public boolean colorGraph(boolean showSol){
		boolean [][] colPos= new boolean[nbV][nbCol];
		for (int i = 0 ; i < nbV ; i++){
			for (int j = 0; j < nbCol ; j++){
				colPos[i][j] = true;
			}
		}
		
		List<AbstractVertex> vert = graph.getVertices();
		Vertex curr;
		for (int i = 0 ; i < nbV ; i++){
			curr = (Vertex) vert.get(i);
			if(curr.getColor() == -1){
				if (naifColorVisite(curr, colPos) == false){
					return false;
				}
			}
		}
		
		if (showSol){
			for (int i = 0 ; i < nbV ; i++){
				curr = (Vertex) vert.get(i);
				System.out.println(curr + " : " + colors[curr.getColor()] );
			}
		}
		return true;
		
	}

	private boolean naifColorVisite(Vertex u, boolean[][]colPos){
		boolean possColoring;
		Vertex currNei;
		List<AbstractVertex> neighs;
		boolean[][] tmp;
		for(int i = 0 ; i < nbCol ; i++){
			possColoring = true;
			if(colPos[u.getId()][i] == true){
				u.setColor(i);
				neighs = u.getNgd();
				for(int j = 0 ; j < neighs.size() ; j++){
					currNei = (Vertex) neighs.get(j);
					if(currNei.getColor() == -1){
						u.addSucc(currNei);
						tmp = copyArray(colPos);
						tmp[currNei.getId()][i] = false;
						if(naifColorVisite(currNei, tmp) == false){
							u.clearSucc();
							possColoring = false;
							break;
						} else {
							colPos = tmp;
						}
					} else if(currNei.getColor() == i){
						u.clearSucc();
						possColoring = false;
						break;
					}
				}
				if (possColoring)
					return true;
			}
		}
		return false;
	}
	
	public boolean[][] copyArray(boolean[][] toCopy){
		boolean[][] copied = new boolean[toCopy.length][toCopy[0].length];
		for (int i = 0 ; i < toCopy.length ; i++){
			copied[i] = Arrays.copyOf(toCopy[i], toCopy[i].length);
		}
		return copied;
	}
}
